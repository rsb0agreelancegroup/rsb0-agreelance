from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from projects.models import Project, Task, ProjectCategory, TaskOffer, Team
from projects.models import Task
from user.models import Profile
from unittest import skip

# Create your tests here.


class ProjectRefactoring(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create_user(username='user1', email="email1@email.com", password='passord1')
        self.user1.is_active = True
        self.user2 = User.objects.create_user(username='user2', email="email2@email.com", password='passord1')
        self.user2.is_active = True
        self.user3 = User.objects.create_user(username='user3', email="email3@email.com", password='passord1')
        self.user3.is_active = True

        self.gardening = ProjectCategory.objects.create()

    def test_create_project(self):
        # Create a new project
        self.client.login(username='user1', password='passord1')
        self.client.post(reverse('new_project'), {
            'title': 'project1',
            'description': 'description1',
            'category_id': 1,
            'task_title': 'task1',
            'task_budget': 100,
            'task_description': 'This is a task!'
        })
        project = Project.objects.get(pk=1)
        task = Task.objects.get(pk=1)
        # Give offer to task
        self.client.login(username='user2', password='passord1')
        self.client.post(reverse("project_view", args="1"), {
            'title': 'Offer1',
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': 1,
            'offer_submit': ''
        })
        # Accept offer to task
        self.client.login(username='user1', password='passord1')
        self.client.post(reverse("project_view", args='1'), {
            'feedback': 'Feedback to offer to task 2',
            'status': 'a',
            'taskofferid': '1',
            'offer_response': ''
        })

        # Create team and add user3 to team
        self.client.login(username='user2', password='passord1')
        self.client.post(reverse('task_view', args=['1', '1']), {
            'name': 'team1',
            'team': ''
        })
        self.client.post(reverse('task_view', args=['1', '1']), {
            'team-id': 1,
            'members': 3,
            'team-add': ''
        })

        # Extract permissions for user1, user2, and user3
        user1_permissions = task.get_user_task_permissions(self.user1.profile)
        user2_permissions = task.get_user_task_permissions(self.user2.profile)
        user3_permissions = task.get_user_task_permissions(self.user3.profile)

        self.assertEqual(user1_permissions, {'write': True, 'read': True, 'offerer': False})
        self.assertEqual(user2_permissions, {'write': True, 'read': True, 'offerer': True})
        self.assertEqual(user3_permissions, {'write': False, 'read': True, 'offerer': False})


        self.assertEqual(project.user.user, self.user1)
        self.assertEqual(task.read.first().user, self.user1)
        self.assertEqual(task.write.first().user, self.user1)
        self.assertTrue(task.read.filter(user_id=2).exists())
        self.assertTrue(task.write.filter(user_id=2).exists())
        self.assertTrue(task.offerer.filter(user_id=2).exists())




class ProjectRefactoringTwo(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create_user(username='user1', email="email1@email.com", password='passord1')
        self.user1.is_active = True
        self.user2 = User.objects.create_user(username='user2', email="email2@email.com", password='passord1')
        self.user1.is_active = True
        self.user3 = User.objects.create_user(username='user3', email="email3@email.com", password='passord1')
        self.user1.is_active = True

        self.gardening = ProjectCategory.objects.create()

        self.project1 = Project.objects.create(
            user=self.user1.profile,
            title="Project1",
            description="This is a project",
            category=self.gardening,
            status='ad'
        )

        self.project1_task1 = Task.objects.create(
            project=Project.objects.get(title="Project1"),
            title='Task1',
            description="This is a task",
            budget=10
        )
        self.project1_task1.read.add(self.user1.profile, self.user2.profile)
        self.project1_task1.write.add(self.user1.profile, self.user2.profile)
        self.project1_task1.offerer.add(self.user2.profile)
        # Create TaskOffer for project
        self.project1_task1_offer = TaskOffer.objects.create(task=self.project1_task1,
                                                             title='Offer project1 task1',
                                                             description="Description for project 2 task 1 offer",
                                                             price=10,
                                                             offerer=self.user2.profile,
                                                             status='a')

    def test_upload_file(self):
        # Assert project owner has access to file upload page
        self.client.login(username='user1', password='passord1')
        response = self.client.get(reverse('upload_file_to_task', args=['1', '1']))
        self.assertEqual(response.status_code, 200)

        # Assert project offerer has access to file upload page
        self.client.login(username='user2', password='passord1')
        response = self.client.get(reverse('upload_file_to_task', args=['1', '1']))
        self.assertEqual(response.status_code, 200)

        # Assert non-team member is redirected when accessing upload page (not allowed to upload files)
        self.client.login(username='user3', password='passord1')
        response = self.client.get(reverse('upload_file_to_task', args=['1', '1']))
        self.assertEqual(response.status_code, 302)

        # Create team and add user3 to team
        self.client.login(username='user2', password='passord1')
        self.client.post(reverse('task_view', args=['1', '1']), {
            'name': 'team1',
            'team': ''
        })
        team = self.project1_task1.teams.first()
        self.client.post(reverse('task_view', args=['1', '1']), {
            'team-id': 1,
            'members': 3,
            'team-add': ''
        })
        # Give team upload access
        self.client.post(reverse('task_view', args=['1', '1']), {
            'permission-upload-1': 'True',
            'permissions': ''
        })

        # Assert user 3 can upload file after being added to team with write access
        self.client.login(username='user3', password='passord1')
        response = self.client.get(reverse('upload_file_to_task', args=['1', '1']))
        self.assertEqual(response.status_code, 200)

        # Check that user3 has read and write access on task, and is a team member
        self.assertEqual(team.members.all().first().user, self.user3)
        self.assertTrue(self.project1_task1.read.filter(user_id=3).exists())
        self.assertTrue(self.project1_task1.write.filter(user_id=3).exists())

    def test_give_task_permissions(self):
        self.client.login(username='user1', password='passord1')
        self.client.post(reverse('task_permissions', args=['1', '1']), {
            'user': '3',
            'permission': 'Read'
        })
        self.client.post(reverse('task_permissions', args=['1', '1']), {
            'user': '3',
            'permission': 'Write'
        })

        self.assertTrue(self.project1_task1.read.filter(user_id=3).exists())
        self.assertTrue(self.project1_task1.write.filter(user_id=3).exists())

    def test_user_has_access_to_task(self):
        # Access task page with user that does not have permissions to view
        self.client.login(username='user3', password='passord1')
        response_one = self.client.get(reverse('task_view', args=['1', '1']))

        # Access task page with user that does have permissions to view
        self.client.login(username='user1', password='passord1')
        response_two = self.client.get(reverse('task_view', args=['1', '1']))

        self.assertEqual(response_one.status_code, 302)
        self.assertEqual(response_one.url, '/user/login')
        self.assertEqual(response_two.status_code, 200)







# =============================================================================================================
# TASK 2:
# Test Statement coverage Project_view function
# =============================================================================================================
class StatementCoverage(TestCase):
    def setUp(self):
        self.client = Client()

        # initiate users.
        self.user1 = User.objects.create_user(username="user1", email="email1@email.com", password="passord1")
        self.user1.is_active = True
        self.user2 = User.objects.create_user(username="user2", email="email2@email.com", password="passord2")
        self.user2.is_active = True


        self.gardening = ProjectCategory.objects.create(name="Gardening")

        # project
        self.project1 = Project.objects.create(
            user=self.user1.profile,
            title="Project1",
            description="This is a project",
            category=self.gardening,
            status='o'
        )

        self.project1_task1 = Task.objects.create(
            project=Project.objects.get(title="Project1"),
            title = 'Task1',
            description="This is a task",
            budget=10
        )
        self.project1_task2 = Task.objects.create(
            project=Project.objects.get(title='Project1'),
            title='Task2',
            description='This is another task',
            budget=20
        )

        # Offer for task 2 from user2
        self.project1_task2_offer = TaskOffer.objects.create(task=self.project1_task2,
                                                             title='Offer project1 task2',
                                                             description='Description for offer for task 2',
                                                             price=10,
                                                             offerer=Profile.objects.get(user=self.user2))

        # Another project owned by user1
        self.project2 = Project.objects.create(
            user=self.user1.profile,
            title="Project2",
            description="This is another project",
            category=self.gardening,
            status='o'
        )
        # Task for project 2
        self.project2_task1 = Task.objects.create(
            project=Project.objects.get(title="Project2"),
            title='Project 2 task 1',
            description="This is a task for project 2",
            budget=10
        )
        # Offer for project2 task 1 from user2
        self.project2_task1_offer = TaskOffer.objects.create(task=self.project2_task1,
                                                             title='Offer project2 task1',
                                                             description="Description for project 2 task 1 offer",
                                                             price=10,
                                                             offerer=self.user2.profile,
                                                             status='a')


    # =============================================================================================================
    # TASK 2:
    # Test Statement coverage Project_view function
    # =============================================================================================================

    def test_total_budget_correct(self):
        total_budget = 0
        for task in self.project1.tasks.all():
            total_budget += int(task.budget)
        response = self.client.get(reverse("project_view", args=str(self.project1.id)))
        self.assertEquals(response.context['total_budget'], total_budget)

    # Test where user 2 posts an offer to task 1
    def test_offer_submit(self):
        self.client.login(username='user2', password='passord2')
        response = self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer 1',
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEquals(response.status_code, 200)

    # Test where user 1 responds to an offer from user 2 on task 2
    def test_offer_response(self):
        self.client.login(username='user1', password='passord1')
        response = self.client.post(reverse("project_view", args='1'), {
            'feedback': 'Feedback to offer to task 2',
            'status': 'a',
            'taskofferid': '1',
            'offer_response': ''
        })
        self.assertEquals(response.status_code, 200)

    # Test where user1 changes status of project 1
    def test_status_change(self):
        self.client.login(username='user1', password='passord1')
        reponse = self.client.post(reverse('project_view', args='1'), {
            'status': 'i',
            'status_change': ''
        })
        self.assertEquals(reponse.status_code, 200)


    # =============================================================================================================
    # Test get_usertask_permission
    # =============================================================================================================
    # (user == task.project.user.user) = True
    """def test_user_equals_task_project_user(self):
        permissions = get_user_task_permissions(self.user1, self.project1_task1)
        for key in permissions:
            self.assertTrue(permissions[key])"""
    # (task.accepted_task_offer and taskoffer.offerer == user.profile) = True
    """def test_user_equals_offerer(self):
        permissions = get_user_task_permissions(self.user2, self.project2_task1)
        for key in permissions:
            if key != 'owner':
                self.assertTrue(permissions[key])
            else:
                self.assertFalse(permissions[key])"""

    """def test_user_not_projcet_owner_or_offerer(self):
        permissions = get_user_task_permissions(self.user2, self.project1_task2)
        for key in permissions:
            self.assertFalse(permissions[key])"""




    # =============================================================================================================
    # TASK 3
    # Test boundary values project offers page
    # =============================================================================================================
class ProjectOfferBoundaryValue(TestCase):
    def setUp(self):
        self.client = Client()

        # initiate users.
        self.user1 = User.objects.create_user(username="user1", email="email1@email.com", password="passord1")
        self.user1.is_active = True
        self.user2 = User.objects.create_user(username="user2", email="email2@email.com", password="passord2")
        self.user2.is_active = True

        self.gardening = ProjectCategory.objects.create(name="Gardening")

        # project
        self.project1 = Project.objects.create(
            user=self.user1.profile,
            title="Project1",
            description="This is a project",
            category=self.gardening,
            status='o'
        )
        self.project1_task1 = Task.objects.create(
            project=Project.objects.get(title="Project1"),
            title = 'Task1',
            description="This is a task",
            budget=10
        )

    #Test title boundry values
    def test_offer_title_empty_string(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': '',
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        # Should not have created entry in database
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 0)

    def test_offer_title_one_character(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'a',
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_title_199_characters(self):
        self.client.login(username='user2', password='passord2')
        title = "a" * 199
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': title,
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_title_200_characters(self):
        self.client.login(username='user2', password='passord2')
        title = "a" * 200
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': title,
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_title_201_characters(self):
        self.client.login(username='user2', password='passord2')
        title = "a" * 201
        response = self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': title,
            'description': 'Description for offer to task 1',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        # Should not have created entry in database
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 0)


    # =============================================================================================================
    # Test description boundry values

    def test_offer_description_empty_string(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': '',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        # Should not have created entry in database
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 0)

    def test_offer_description_one_character(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': 'a',
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_description_499_character(self):
        self.client.login(username='user2', password='passord2')
        description = "a" * 499
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': description,
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_description_500_character(self):
        self.client.login(username='user2', password='passord2')
        description = "a" * 500
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': description,
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_description_501_character(self):
        self.client.login(username='user2', password='passord2')
        description = "a" * 501
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': description,
            'price': '10',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        # Should not have created entry in database
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 0)


    # =============================================================================================================
    # Test price boundry values

    def test_offer_price_empty_string(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': 'Description',
            'price': '',
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        # Should not have created entry in database
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 0)

    def test_offer_price_zero(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': 'Description',
            'price': 0,
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    def test_offer_price_one(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': 'description',
            'price': 1,
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertEqual(self.project1_task1.taskoffer_set.count(), 1)

    @skip('Should not pass according to https://docs.djangoproject.com/en/3.0/ref/models/fields/#django.db.models.IntegerField')
    def test_offer_price_max(self):
        def test_offer_price_one(self):
            self.client.login(username='user2', password='passord2')
            self.client.post(reverse("project_view", args=str(self.project1.id)), {
                'title': 'Offer',
                'description': 'description',
                'price': 2147483648,
                'taskvalue': self.project1_task1.id,
                'offer_submit': ''
            })
            self.assertNotEqual(self.project1_task1.taskoffer_set.all().first().price, 2147483648)

    @skip('This test should fail, but passes. Offering price of -1 makes no sence')
    def test_offer_price_negative(self):
        self.client.login(username='user2', password='passord2')
        self.client.post(reverse("project_view", args=str(self.project1.id)), {
            'title': 'Offer',
            'description': 'description',
            'price': -1,
            'taskvalue': self.project1_task1.id,
            'offer_submit': ''
        })
        self.assertNotEqual(self.project1_task1.taskoffer_set.first().price, -1)
    # =============================================================================================================
    """***************************************************************************************************************
                        Boundry value tests for signup page is in /users/tests.py
    ***************************************************************************************************************"""


    # =============================================================================================================
    # Test output coverage accept offers page
    # =============================================================================================================
class OutputCoverage(TestCase):

    def setUp(self):
        self.client = Client()

        # initiate users.
        self.user1 = User.objects.create_user(username="user1", email="email1@email.com", password="passord1")
        self.user1.is_active = True
        self.user2 = User.objects.create_user(username="user2", email="email2@email.com", password="passord2")
        self.user2.is_active = True

        self.user3 = User.objects.create_user(username="user3", email="email3@email.com", password="passord3")
        self.user3.is_active = True

        self.gardening = ProjectCategory.objects.create(name="Gardening")

        # project
        self.project1 = Project.objects.create(
            user=self.user1.profile,
            title="Project1",
            description="This is a project",
            category=self.gardening,
            status='o'
        )

        self.project1_task2 = Task.objects.create(
            project=Project.objects.get(title='Project1'),
            title='Task2',
            description='This is another task',
            budget=20
        )

        # Create an offer for task 2 from user2
        self.project1_task2_offer = TaskOffer.objects.create(task=self.project1_task2,
                                                             title='Offer project1 task2',
                                                             description='Description for offer for task 2',
                                                             price=10,
                                                             offerer=Profile.objects.get(user=self.user2))


    # =============================================================================================================
    # Begin testing

    # Test output coverage where user1 responds 'accept' to offer from user2
    """def test_offer_respond_accept_output(self):
        self.client.login(username='user1', password='passord1')

        feedback = 'Feedback to offer to task 2'
        status = 'a'
        self.client.post(reverse("project_view", args='1'), {
            'feedback': feedback,
            'status': status,
            'taskofferid': '1',
            'offer_response': ''
        })
        user1_permissions = get_user_task_permissions(self.user1, self.project1_task2)
        user2_permissions = get_user_task_permissions(self.user2, self.project1_task2)
        user3_permissions = get_user_task_permissions(self.user3, self.project1_task2)

        # Assert taskoffer status and feedback is set correctly
        self.assertEquals(self.project1_task2.taskoffer_set.all().first().status, status)
        self.assertEquals(self.project1_task2.taskoffer_set.all().first().feedback, feedback)

        # Assert permissions correctly set for user2 in task.read, task.write, task.modify
        self.assertIn(self.user2.profile, self.project1_task2.read.all())
        self.assertIn(self.user2.profile, self.project1_task2.write.all())
        self.assertNotIn(self.user2.profile, self.project1_task2.modify.all())

        # Assert permissions correctly returned from get_user_task_permissions() for User 1 (project owner):
        # Expected output: {True, True, True, True, True}
        self.assertTrue(user1_permissions['read'])
        self.assertTrue(user1_permissions['write'])
        self.assertTrue(user1_permissions['modify'])
        self.assertTrue(user1_permissions['owner'])
        self.assertTrue(user1_permissions['upload'])


        # Assert permissions correctly returned from get_user_task_permissions() for User 2 (offerer):
        # Expected output: {True, True, modify=False, owner=False, True}
        self.assertTrue(user2_permissions['read'])
        self.assertTrue(user2_permissions['write'])

        # **************************************
        # Bug: this should be false, or project1_task2.modify() should be false as well:
        self.assertTrue(user2_permissions['modify'])
        # **************************************

        self.assertFalse(user2_permissions['owner'])
        self.assertTrue(user2_permissions['upload'])


        # Check permissions for user who is not offerer or project owner.
        # Expected output: {False, False, False, False, False}
        self.assertFalse(user3_permissions['read'])
        self.assertFalse(user3_permissions['write'])
        self.assertFalse(user3_permissions['modify'])
        self.assertFalse(user3_permissions['owner'])
        self.assertFalse(user3_permissions['upload'])
        """
    # =============================================================================================================

    # Test output coverage where user1 responds 'decline' to offer from user2
    """ def test_offer_respond_denied_output(self):
        self.client.login(username='user1', password='passord1')
        feedback = 'Feedback to offer to task 2'
        status = 'd'
        self.client.post(reverse("project_view", args='1'), {
            'feedback': feedback,
            'status': status,
            'taskofferid': '1',
            'offer_response': ''
        })
        permissions = get_user_task_permissions(self.user2, self.project1_task2)

        # Assert taskoffer status and feedback is set correctly
        self.assertEquals(self.project1_task2.taskoffer_set.all().first().status, status)
        self.assertEquals(self.project1_task2.taskoffer_set.all().first().feedback, feedback)

        # Assert permissions correctly set for user2 in task.read, task.write, task.modify
        self.assertNotIn(self.user2.profile, self.project1_task2.read.all())
        self.assertNotIn(self.user2.profile, self.project1_task2.write.all())
        self.assertNotIn(self.user2.profile, self.project1_task2.modify.all())

        # Assert permissions correctly returned from get_user_task_permissions()
        self.assertFalse(permissions['read'])
        self.assertFalse(permissions['write'])
        self.assertFalse(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertFalse(permissions['upload'])"""


    # =============================================================================================================

    # Test output coverage where user1 responds 'decline' to offer from user2
    """def test_offer_respond_pening_output(self):
        self.client.login(username='user1', password='passord1')

        feedback = 'Feedback to offer to task 2'
        status = 'p'
        self.client.post(reverse("project_view", args='1'), {
            'feedback': feedback,
            'status': status,
            'taskofferid': '1',
            'offer_response': ''
        })
        permissions = get_user_task_permissions(self.user2, self.project1_task2)

        # Assert taskoffer status and feedback is set correctly
        self.assertEquals(self.project1_task2.taskoffer_set.all().first().status, status)
        self.assertEquals(self.project1_task2.taskoffer_set.all().first().feedback, feedback)

        # Assert permissions correctly set for user2 in task.read, task.write, task.modify
        # Expected: {NotIn, NotIn, NotIn}
        self.assertNotIn(self.user2.profile, self.project1_task2.read.all())
        self.assertNotIn(self.user2.profile, self.project1_task2.write.all())
        self.assertNotIn(self.user2.profile, self.project1_task2.modify.all())

        # Assert permissions correctly returned from get_user_task_permissions()
        # Expected: {False, False, False, False, False}
        self.assertFalse(permissions['read'])
        self.assertFalse(permissions['write'])
        self.assertFalse(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertFalse(permissions['upload'])"""
