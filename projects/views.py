from django.http import HttpResponse, HttpResponseRedirect
from user.models import Profile
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, \
    TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.contrib import messages
from django.core import mail


def projects(request):
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request,
                  'projects/projects.html',
                  {
                      'projects': projects,
                      'project_categories': project_categories,
                  }
                  )


@login_required
def new_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = request.user.profile
            project.category = get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
            project.save()

            task_title = request.POST.getlist('task_title')
            task_description = request.POST.getlist('task_description')
            task_budget = request.POST.getlist('task_budget')
            for i in range(0, len(task_title)):
                Task.objects.create(
                    title=task_title[i],
                    description=task_description[i],
                    budget=task_budget[i],
                    project=project,
                )
            for task in project.tasks.all():
                task.set_task_permissions(profile=request.user.profile, read=True, write=True, offerer=False)
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})


def new_project_email(request, project):
    current_site = get_current_site(request)
    people = Profile.objects.filter(categories__id=project.category.id)
    for person in people:
        if person.user.email:
            try:
                with mail.get_connection() as connection:
                    mail.EmailMessage(
                        "New Project: " + project.title,
                        "A new project you might be interested in was created and can be viwed at " + current_site.domain + '/projects/' + str(
                            project.id), "Agreelancer", [person.user.email],
                        connection=connection,
                    ).send()
            except Exception as e:
                messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = total_budget_from_tasks(tasks)

    if is_project_owner(request.user, project):

        if request.method == 'POST' and 'offer_response' in request.POST:
            project = request_handle_offer_response(request, project)
        offer_response_form = TaskOfferResponseForm()

        if request.method == 'POST' and 'status_change' in request.POST:
            request_handle_status_change(request, project)
        status_form = ProjectStatusForm(initial={'status': project.status})

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'status_form': status_form,
            'total_budget': total_budget,
            'offer_response_form': offer_response_form,
        })

    else:
        return request_handle_submit(request, project, tasks, total_budget)


def total_budget_from_tasks(tasks):
    total_budget = 0
    for item in tasks:
        total_budget += item.budget
    return total_budget


def request_handle_offer_response(request, project):
    instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
    offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
    if offer_response_form.is_valid():
        offer_response = offer_response_form.save(commit=False)

        if offer_response.status == 'a':
            offer_response.task.set_task_permissions(profile=offer_response.offerer, read=True, write=True, offerer=True)
            project = offer_response.task.project
            project.participants.add(offer_response.offerer)

        offer_response.save()
    return project


def request_handle_status_change(request, project):
    status_form = ProjectStatusForm(request.POST)
    if status_form.is_valid():
        project_status = status_form.save(commit=False)
        project.status = project_status.status
        project.save()


def request_handle_submit(request, project, tasks, total_budget):
    if request.method == 'POST' and 'offer_submit' in request.POST:
        task_offer_form = TaskOfferForm(request.POST)
        if task_offer_form.is_valid():
            task_offer = task_offer_form.save(commit=False)
            task_offer.task = Task.objects.get(pk=request.POST.get('taskvalue'))
            task_offer.offerer = request.user.profile
            task_offer.save()

    task_offer_form = TaskOfferForm()
    return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'task_offer_form': task_offer_form,
        'total_budget': total_budget
    })


def is_project_owner(user, project):
    return user == project.user.user


@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = task.get_user_task_permissions(request.user.profile)

    if user_permissions['write']:
        if request.method == 'POST':
            task_file_form = TaskFileForm(request.POST, request.FILES)
            if task_file_form.is_valid():
                task_file = task_file_form.save(commit=False)
                task_file.task = task
                existing_file = task.files.filter(file=directory_path(task_file, task_file.file.file)).first()
                if existing_file:
                    existing_file.delete()
                task_file.save()

                if not is_project_owner(request.user, project) and not user_permissions['offerer']:
                    teams = request.user.profile.teams.filter(task__id=task.id)
                    set_file_team_permissions(teams, task_file)

                return redirect('task_view', project_id=project_id, task_id=task_id)

        task_file_form = TaskFileForm()
        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': task_file_form,
            }
        )
    return redirect('/user/login')  # Redirects to /user/login


def set_file_team_permissions(teams, task_file):
    for team in teams:
        tft = TaskFileTeam()
        tft.team = team
        tft.file = task_file
        tft.read = True
        tft.save()



@login_required
def task_view(request, project_id, task_id):
    user = request.user
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    accepted_task_offer = task.accepted_task_offer()

    user_permissions = task.get_user_task_permissions(user.profile)
    if not user_permissions['read'] and not user_permissions['write']:
        return redirect('/user/login')

    if request.method == 'POST' and 'delivery' in request.POST:
        if accepted_task_offer and user_permissions['offerer']:
            deliver_form = DeliveryForm(request.POST, request.FILES)
            if deliver_form.is_valid():
                delivery = deliver_form.save(commit=False)
                delivery.task = task
                delivery.delivery_user = user.profile
                delivery.save()
                task.status = "pa"
                task.save()

    if request.method == 'POST' and 'delivery-response' in request.POST:
        instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
        deliver_response_form = TaskDeliveryResponseForm(request.POST, instance=instance)
        if deliver_response_form.is_valid():
            delivery = deliver_response_form.save()
            from django.utils import timezone
            delivery.responding_time = timezone.now()
            delivery.responding_user = user.profile
            delivery.save()

            if delivery.status == 'a':
                task.status = "pp"
                task.save()
            elif delivery.status == 'd':
                task.status = "dd"
                task.save()

    if request.method == 'POST' and 'team' in request.POST:
        if accepted_task_offer and user_permissions['offerer']:
            team_form = TeamForm(request.POST)
            if (team_form.is_valid()):
                team = team_form.save(False)
                team.task = task
                team.save()

    if request.method == 'POST' and 'team-add' in request.POST:
        if accepted_task_offer and user_permissions['offerer']:
            instance = get_object_or_404(Team, id=request.POST.get('team-id'))
            team_add_form = TeamAddForm(request.POST, instance=instance)
            if team_add_form.is_valid():
                team = team_add_form.save(False)
                team.members.add(*team_add_form.cleaned_data['members'])
                task.read.add(*team_add_form.cleaned_data['members']) # give new team members read-permission on task
                team.save()

    if request.method == 'POST' and 'permissions' in request.POST:
        if accepted_task_offer and user_permissions['offerer']:
            for t in task.teams.all():
                for f in task.files.all():
                    try:
                        tft_string = 'permission-perobj-' + str(f.id) + '-' + str(t.id)
                        tft_id = request.POST.get(tft_string)
                        instance = TaskFileTeam.objects.get(id=tft_id)
                    except Exception as e:
                        instance = TaskFileTeam(
                            file=f,
                            team=t,
                        )

                    instance.read = request.POST.get('permission-read-' + str(f.id) + '-' + str(t.id)) or False
                    instance.modify = request.POST.get('permission-modify-' + str(f.id) + '-' + str(t.id)) or False
                    instance.save()
                t.write = request.POST.get('permission-upload-' + str(t.id)) or False
                if request.POST.get('permission-upload-' + str(t.id)):
                    for member in t.members.all():
                        task.set_task_permissions(profile=member, write=True)
                else:
                    for member in t.members.all():
                        member_permissions = task.get_user_task_permissions(member)
                        if not is_project_owner(request.user, project) and not member_permissions['offerer']:
                            task.write.remove(member)
                t.save()

    deliver_form = DeliveryForm()
    deliver_response_form = TaskDeliveryResponseForm()
    team_form = TeamForm()
    team_add_form = TeamAddForm()


    deliveries = task.delivery.all()
    team_files = []
    teams = user.profile.teams.filter(task__id=task.id).all()
    per = {}
    for f in task.files.all():
        per[f.name()] = {}
        for p in f.teams.all():
            per[f.name()][p.team.name] = p
            if p.read:
                team_files.append(p)
    return render(request, 'projects/task_view.html', {
        'task': task,
        'project': project,
        'user_permissions': user_permissions,
        'deliver_form': deliver_form,
        'deliveries': deliveries,
        'deliver_response_form': deliver_response_form,
        'team_form': team_form,
        'team_add_form': team_add_form,
        'team_files': team_files,
        'per': per
    })


@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    user_permissions = task.get_user_task_permissions(user.profile)
    if is_project_owner(request.user, project) or user_permissions['offerer']:
        task = Task.objects.get(pk=task_id) # what?
        if int(project_id) == task.project.id:
            if request.method == 'POST':
                task_permission_form = TaskPermissionForm(request.POST)
                if task_permission_form.is_valid():
                    try:
                        username = task_permission_form.cleaned_data['user']
                        user = User.objects.get(username=username)
                        permission_type = task_permission_form.cleaned_data['permission']
                        if permission_type == 'Read':
                            task.set_task_permissions(profile=user.profile, read=True)
                        elif permission_type == 'Write':
                            task.set_task_permissions(user.profile, write=True)
                    except (Exception):
                        print("user not found")
                    return redirect('task_view', project_id=project_id, task_id=task_id)

            task_permission_form = TaskPermissionForm()
            return render(
                request,
                'projects/task_permissions.html',
                {
                    'project': project,
                    'task': task,
                    'form': task_permission_form,
                }
            )
    return redirect('task_view', project_id=project_id, task_id=task_id)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
