# Generated by Django 2.1.7 on 2020-03-09 18:24

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user', '0002_profilerating'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='rating',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.DO_NOTHING, to='user.ProfileRating'),
        ),
        migrations.AddField(
            model_name='profilerating',
            name='rating_for',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.DO_NOTHING, related_name='rating_for', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='profilerating',
            name='rating_from',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.DO_NOTHING, related_name='rating_from', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='profilerating',
            name='rating',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)]),
        ),
    ]
