from allpairspy import AllPairs


def generate_all_pairs_combination():
    """
    Prints a all pairs combinations of all valid
    """
    user_name = {"UserOne": True, "UserTwo": True, "UserOne{}": False}
    first_name = {"Christopher": True, "Christopher1": True}
    last_name = {"Smith": True, "Smith1": True}
    company_name = {"Megacorp": True, "": True}
    categories = [1, 2, 3]
    email = {"email@gmail.com": True, "gmail.com": False}
    conf_email = {"email@gmail.com": True, "gmail.com": False}
    password = {"passord1": True, "pass": False}
    password_confirm = {"passord1": True, "pass": False}
    phone_number = {"81549300": True, "Phone number": False}
    countries = {
        'Norway': {'State': 'N/A', 'City': 'Trondheim', 'Postal code': '7013', 'Street address': 'Munkegata 1'},
        'Sweden': {'State': 'N/A', 'City': 'Stockholm', 'Postal code': '111 52', 'Street address': 'Jakobsgatan 10'},
        'USA': {'State': 'Minnesota', 'City': 'Minneapolis', 'Postal code': '55415',
                'Street address': '401 Chicago Avenue'}
    }
    parameters = [
        list(user_name.keys()),
        list(first_name.keys()),
        list(last_name.keys()),
        list(company_name.keys()),
        categories,
        list(email.keys()),
        list(conf_email.keys()),
        list(password.keys()),
        list(password_confirm.keys()),
        list(phone_number.keys()),
        list(countries.keys()),
        [countries['Norway']['State'], countries['Sweden']['State'], countries['USA']['State']],
        [countries['Norway']['City'], countries['Sweden']['City'], countries['USA']['City']],
        [countries['Norway']['Postal code'], countries['Sweden']['Postal code'], countries['USA']['Postal code']],
        [countries['Norway']['Street address'], countries['Sweden']['Street address'], countries['USA']['Street address']]
    ]
    combinations = []
    i = 0
    for pairs in enumerate(AllPairs(parameters)):
        combinations.append(list(pairs[1]))
    for combination in combinations:
        valid_form = False
        # check if this is a valid form
        if user_name[combination[0]] and first_name[combination[1]] and last_name[combination[2]] \
                and company_name[combination[3]] and email[combination[5]] and conf_email[combination[6]] \
                and combination[5] == combination[6] and password[combination[7]] and password_confirm[combination[8]] \
                and combination[7] == combination[8] and phone_number[combination[9]] \
                and countries[combination[10]]['City'] == combination[12] \
                and countries[combination[10]]['Postal code'] == combination[13] \
                and countries[combination[10]]['Street address'] == combination[14]:
            valid_form = True
        i += 1
        print("{}: {} {}".format(i, combination, "valid_form" if valid_form else "invalid form"))

generate_all_pairs_combination()